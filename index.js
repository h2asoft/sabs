/** @format */

// import {AppRegistry} from 'react-native';
// import App from './app/components/Tickets';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import Tickets from './app/components/Tickets-Redux';

const store = configureStore()

export default class App extends Component {

    render() {
        return (
            <Provider store={store}>
                <Tickets />
            </Provider>
        );
    }
}
import { combineReducers } from 'redux'
import {
    GET_TICKETS
} from '../actions';


const defaultState = {
  tickets: []
}
  
const ticketsReducer = (state = defaultState, action) => {
    switch (action.type) {
        case GET_TICKETS:
        return {
            ...state,
            tickets: action.data.movies
        };
        
        default:    
        return state;
    }
};

const rootReducer = combineReducers({
  ticketsData : ticketsReducer
});

export default rootReducer;

export const GET_TICKETS = 'GET_TICKETS'


export function getTickets(json) {
  return {
    type: GET_TICKETS,
    tickets: [
                {
                'key': '1st', 
                'data': [ json[0] ]
                },
                {
                'key': '2nd', 
                'data': [ json[1] ]
                }
            ]
  }
}
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Text, View, Image, SectionList } from 'react-native';
import styles from '../styles/style';
import array from '../assets/results.json';

var json = [
  {
    'key': '1st', 
    'data': [ array[0] ]
  },
  {
    'key': '2nd', 
    'data': [ array[1] ]
  }
]

export default class Tickets extends Component {
  
  //Render Title
  _renderItem = ({item}) => (
    <View style={styles.sectionStops}>
      <Text style={styles.sublist} >Departure : {`${item.DepStationCodeFull}`} | {item.DepTime} </Text>
      <View>
        {this.renderStops(item.Legs)}
      </View>
    </View>
  )

  //Render Stops
  renderStops(stops) {
    return stops.map((item, index) => {
        { stops.length === index + 1 ? str = "Arrival" :  str = "Stop"}
        return (
            <View>
              <View>
                  {this.renderTickets(item.TKTs)}
              </View>
              <Text style={styles.sublist} >
                  {str}: {item.ArrStnFull} | {item.ArrTime}
              </Text>
            </View>
            
        );
    });
  }

  //Render Tickets Prices
  renderTickets(tickets) {
    return tickets.map((item, index) => {
      if(item.AdtPrice != null){
        price = <Text style={styles.boldSubList} > £{item.AdtPrice} - {item.TicketDescription} </Text>
      }else{
        price = null
      }
      return (
        price
      );
    });
  }

  //Render Header Name
  _renderSectionHeader = ({section}) => {
      return (
        <View style={styles.sectionHeader}>
          <Text style={styles.header}> {`${section.key}`} </Text>
        </View>
      )
  }
  
  render() {
    return (
      <View style={styles.ticketsContainer}>
        <View style={styles.imgContainer} >
          <Image  source={require("../assets/logo.png")} ></Image>
        </View>
        <SectionList
            sections={json}
            renderItem={this._renderItem}
            renderSectionHeader={this._renderSectionHeader}
            keyExtractor={(item, index) => index.toString()}
        />
        
      </View>
    );
  }
}

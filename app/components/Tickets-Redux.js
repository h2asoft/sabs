/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Text, View, Image, SectionList } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTickets } from '../actions';
import styles from '../styles/style';
import array from '../assets/results.json';

class Tickets extends Component {

  componentDidMount() {
    this.props.getTickets(array);
  }

  
  //Render Title
  _renderItem = ({item}) => (
    <View style={styles.sectionStops}>
      <Text style={styles.sublist} >Departure : {`${item.DepStationCodeFull}`} | {item.DepTime} </Text>
      <View>
        {this.renderStops(item.Legs)}
      </View>
    </View>
  )

  //Render Stops
  renderStops(stops) {
    return stops.map((item, index) => {
        { stops.length === index + 1 ? str = "Arrival" :  str = "Stop"}
        return (
            <View>
              <View>
                  {this.renderTickets(item.TKTs)}
              </View>
              <Text style={styles.sublist} >
                  {str}: {item.ArrStnFull} | {item.ArrTime}
              </Text>
            </View>
            
        );
    });
  }

  //Render Tickets Prices
  renderTickets(tickets) {
    return tickets.map((item, index) => {
      if(item.AdtPrice != null){
        price = <Text style={styles.boldSubList} > £{item.AdtPrice} - {item.TicketDescription} </Text>
      }else{
        price = null
      }
      return (
        price
      );
    });
  }

  //Render Header Name
  _renderSectionHeader = ({section}) => {
      return (
        <View style={styles.sectionHeader}>
          <Text style={styles.header}> {`${section.key}`} </Text>
        </View>
      )
  }
  
  render() {
    const { tickets } = this.props.tickets
    return (
      <View style={styles.ticketsContainer}>
        <View style={styles.imgContainer} >
          <Image  source={require("../assets/logo.png")} ></Image>
        </View>
        <SectionList
            sections={tickets}
            renderItem={this._renderItem}
            renderSectionHeader={this._renderSectionHeader}
            keyExtractor={(item, index) => index.toString()}
        />
        
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
      tickets: state.tickets
  }
}

function mapDispatchToProps(dispatch) {
  return {
      ...bindActionCreators({ getTickets }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tickets)
const React = require("react-native");
const { Dimensions } = React;

const width  = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


export default {

    imgContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    ticketsContainer: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        paddingTop: 40
    },
    sectionHeader: {
        height: 50,
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        paddingLeft: 10,
        marginBottom: 10,
        marginTop: 10,
    },
    header: {
        fontSize: 20,
    },
    sectionStops: {
        justifyContent: 'center',
        paddingLeft: 20
    },
    sublist:{
        fontSize: 16,
        marginBottom: 5,
    },
    boldSubList:{
        fontWeight: "bold",
        marginBottom: 5,
    }
};

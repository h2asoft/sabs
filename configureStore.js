import { createStore, applyMiddleware } from 'redux';
import app from './app/reducers';

export default function configureStore() {
    let store = createStore(app)
    return store
}